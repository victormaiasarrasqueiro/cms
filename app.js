var express         = require('express');
var path            = require('path');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var helmet          = require('helmet');
var cors            = require('cors'); 
var requestIp       = require('request-ip');
var jwt             = require('jsonwebtoken');

var routes          = require('./routes/index');
var artigos         = require('./routes/artigos');
var usuario         = require('./routes/usuario');
var categoria       = require('./routes/categoria');
var tagsbad         = require('./routes/tagsbad');
var imagem          = require('./routes/imagem');
var authenticate    = require('./routes/authenticate');

// Importando configurações do sistema.
var config          = require('./config');




var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit:'50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//Habilita CORS
var corsOptions = {
  origin: ['http://localhost'],
  methods:['GET','POST','PUT','DELETE'],
  allowedHeaders:['Content-Type', 'Authorization','h_site_idx']
};

app.use(cors(corsOptions));

//helmet ( Protenção HTTP - X-Content-Type-Options , X-DNS-Prefetch-Control, X-Download-Options, X-Frame-Options, X-XSS-Protection)
app.use(helmet());

// Desabilitando cache no servidor
// he ETag or entity tag is part of HTTP, the protocol for the World Wide Web. 
// It is one of several mechanisms that HTTP provides for web cache validation
app.disable('etag');

// Recuperando informações de rede do usuário.
app.use(requestIp.mw());

app.use(function(req, res, next) {

	console.log("foi");
	var h_site_id = req.headers.h_site_idx;
	console.log(h_site_id);

	next();

});

// Rotas publicas
app.use('/', routes);
app.use('/authenticate', authenticate);



// route middleware to verify a token
app.use(function(req, res, next) {

  try{

    // Recuperando o parâmetro no Header
    var auth  = req.headers.authorization;
    var token = req.headers.authorization.split(' ')[1];
    
    if (token) {

      jwt.verify(token, config.privateKey, function(err, decoded) {      
     
        if (err) {
          
          return res.json({ success: false, code:401, message: 'Failed to authenticate token.' });    
        
        } else {
          
          req.decoded = decoded;
          next();
        
        }

      });

    } else {
      // if there is no token return an error
      throw 'No token provided.';
    }


  }catch(e){
    
    return res.json({ success: false, code:403, message: 'No token provided' });    

  }

});



// Rotas privadas
app.use('/artigos', artigos);
app.use('/usuario', usuario);
app.use('/categoria', categoria);
app.use('/tagsbad', tagsbad);
app.use('/imagem', imagem);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);

    console.log(err.message);

    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  
  console.log(err.message);

  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(3000,function(){

  console.log("Sistema Iniciado");
  
})

module.exports = app;
