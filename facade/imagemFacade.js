var conn = require('./../conn');
var fs = require('fs');

var AWS = require('aws-sdk'); 
var Sync = require('sync');
var cluster = require('cluster');

var facade = {};


/* GET home page. */
facade.inserirImagem = function(imagemBase64, callback) {
	
	try{

		conn.getConnection(function(err,conn){


		  	if(err){
				throw err;
		  	}
		  	else
		  	{

				conn.beginTransaction(function(err) {


					if(err){
						throw err;
					}

					conn.query('INSERT INTO tb_imagem SET ?', { type:imagemBase64.filetype, size:imagemBase64.filesize, filename:imagemBase64.filename }, function(err, imagem) {
	  
	  
						if (err) {
					   
					      return conn.rollback(function() {
					        throw err;
					      });
					    
					    }else{

					  		saveImageFileServer(imagemBase64,imagem.insertId,function(err,msg){

					  			if(err){

					  				return conn.rollback(function() {
						            	throw err;
						          	});

					  			}else{


					  				
					  				conn.commit(function(err) {
				        				
				        				if (err) {
								          
								          return conn.rollback(function() {
								            throw err;
								          });

								        }else{

								        	if(conn){
									  			conn.release();
									  		}

								        	callback(null,imagem.insertId);

								        };
								        

							      	}); // Fim do Commit



					  			}

					  		});

							
						} // fim do else

	  
					}); // Fim do primeiro INSERT


				});

			}//Fim do Else


		});


	}catch(e){

		callback(e,null);

	}


};

facade.inserirListaImagem = function(arrayImagemBase64, callback) {
	
	var erro = null;

	Sync(function(){

		try{

			for(var i = 0; i<arrayImagemBase64.length; i++){

				var result = facade.inserirImagem.sync(null,arrayImagemBase64[i]);

			}

		}catch(e){
			erro = e;
			console.log(e);

		}

	});

	if(erro){
		callback(erro,null);
	}else{
		callback(null,"SUCESSO");
	}


};


facade.getListaImagem = function(idUsuario, callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql  = "SELECT * FROM tb_imagem WHERE usuario = " + idUsuario;

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

			}//Fim do Else

		});

	}catch(e){

		callback(err,null);

	}
	
};


// function to create file from base64 encoded string
function saveImageFileServer(base64Img,idImage,callback) {

	try{

		
		
		var decodedImage = new Buffer(base64Img.base64, 'base64');
		
		// Set your region for future requests.
		AWS.config.region = 'us-west-2';

    	var s3bucket = new AWS.S3({params: {Bucket: 'mestrejedi'}});

    	s3bucket.createBucket(function() {
		  
		  var params = {Key: 'imagens/' + idImage + '.jpg', Body: decodedImage};
		  
		  s3bucket.upload(params, function(err, data) {
		    
		    if (err) {
		      
	      		callback(err,null);

		    } else {


	    		callback(null,'Sucesso!');
		      
		    }

		  });

		});
    	
	
	}catch(e){
		console.log(e);
		callback(e,null);
	}
    
}


module.exports = facade;
