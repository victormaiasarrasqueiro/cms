var conn = require('./../conn');

var facade = {};


facade.getTagsArtigo = function(idArtigo, callback) {
	
	
	conn.getConnection(function(err,conn){

	  	if(err){
			throw err;
	  	}else{


	  		var sql    = "SELECT * FROM tb_artigo_tags WHERE tb_artigo_tags.id_artigo = "  + idArtigo;

			var query = conn.query(sql, function(err, results) {

				if(err){

					conn.release();
					callback(err,null);
				}

				conn.release();
				
				callback(null,results);
					
			});

			
		}//Fim do Else


	});

};

facade.saveTagsArtigo = function(idArtigo, tags, callback) {
	
	
	conn.getConnection(function(err,conn){

	  	if(err){
			throw err;
	  	}else{


	  		conn.beginTransaction(function(err) {


				if(err){
					throw err;
				}

				var sql    = "DELETE FROM tb_artigo_tags WHERE id_artigo = "  + idArtigo;

				conn.query(sql, function(err, results) {


					if (err) {
				      
				      return conn.rollback(function() {
				        throw err;
				      });
				    
				    }else{

				    	var sqlInsert = " INSERT INTO tb_artigo_tags ( id_artigo, text ) VALUES ";
						
						for(var l = 0 ; l<tags.length ; l++ ){

							sqlInsert = sqlInsert + " ( " +  idArtigo + " , " + "'" + tags[l].text + "'" + " ) ";
							if(l != tags.length - 1){
								sqlInsert = sqlInsert + ",";
							}


						}

						conn.query(sqlInsert, {}, function(err, results) {
			  
			  
							if (err) {
				      
						      return conn.rollback(function() {
						        throw err;
						      });
						    
						    }else{

							    conn.commit(function(err) {
	        				
			        				
			        				if (err) {
							          
							          return conn.rollback(function() {
							            throw err;
							          });

							        }else{

							       
							        	if(conn){
								  			conn.release();
								  		}

							    		callback(null,results);

							        };

							        

						      	}); // Fim do Commit

							} // Fim do Else

			  
						}); // Fim do segundo INSERT


					} // fim do else

	  
				}); // Fim do primeiro INSERT
			    			

			}); // fim do begin transaction

			
		}//Fim do Else


	});

};




module.exports = facade;
