var conn = require('./../conn');

var facade = {};

/* GET home page. */
facade.getListaCategoria = function(callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_categoria WHERE pai = 0 order by id";

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

				
			}//Fim do Else


		});


	}catch(e){

		callback(err,null);

	}
	

};


module.exports = facade;
