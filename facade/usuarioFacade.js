var conn = require('./../conn');

var facade = {};


/* GET home page. */
facade.getListaUsuario = function(callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_usuario";

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

				


			}//Fim do Else


		});


	}catch(e){

		callback(err,null);

	}
	

};

facade.getUsuarioPorEmail = function(email, callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_usuario WHERE email = '" + email + "'";

		  	

				var query = conn.query(sql, function(err, results) {

					if(err){
						console.log(err);
						conn.release();
						callback(err,null);
					}


					conn.release();
					
					if(results && results.length > 0){
						callback(null,results[0]);
					}else{
						callback(null,null);
					}
					
						
				});

				


			}//Fim do Else


		});


	}catch(e){
		console.log(e);

		callback(err,null);

	}
	

};


module.exports = facade;
