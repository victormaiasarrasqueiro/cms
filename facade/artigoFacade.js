var conn = require('./../conn');

var facade = {};


/* GET home page. */
facade.getArtigoPorPagina = function(url, callback) {
	
	
	conn.getConnection(function(err,conn){

	  	if(err){
			throw err;
	  	}else{


	  		var sql    = "SELECT * FROM tb_artigo, tb_pagina WHERE tb_artigo.id = tb_pagina.id_artigo AND tb_pagina.url = '"  + url.toLowerCase() + "'";

			var query = conn.query(sql, function(err, results) {

				if(err){

					conn.release();
					callback(err,null);
				}

				conn.release();
				
				callback(null,results);
					
			});

			
		}//Fim do Else


	});

};

facade.getArtigo = function(idArtigo, callback) {
	
	
	conn.getConnection(function(err,conn){

	  	if(err){
			throw err;
	  	}else{


	  		var sql    = "SELECT * FROM tb_artigo WHERE tb_artigo.id = "  + idArtigo;

			var query = conn.query(sql, function(err, results) {

				if(err){

					conn.release();
					callback(err,null);
				}

				conn.release();
				
				callback(null,results);
					
			});

			
		}//Fim do Else


	});

};

/* GET home page. */
facade.getListaArtigoPorUsuario = function(idUsuario,callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_artigo WHERE usuario = " + idUsuario;

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

			}//Fim do Else

		});


	}catch(e){

		callback(err,null);

	}
	

};

/* GET home page. */
facade.getListaArtigo = function(callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT id,titulo FROM tb_artigo ";

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

			}//Fim do Else

		});


	}catch(e){

		callback(err,null);

	}
	

};

/* GET home page. */
facade.getListaArtigoPorCategoria = function(idCategoria,callback) {
	
	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_artigo WHERE categoria = " + idCategoria;

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						callback(err,null);
					}


					conn.release();
					
					callback(null,results);
						
				});

			}//Fim do Else

		});


	}catch(e){

		callback(err,null);

	}
	

};

module.exports = facade;
