
var mysql = require('mysql');

module.exports = {


	pool: mysql.createPool({
		connectionLimit : 20,
    	connectTimeout  : 30000,
    	aquireTimeout   : 40000,
    	timeout         : 50000,
	  	host     : 'dev.crbbpavnsk2h.us-west-2.rds.amazonaws.com',
	  	port     : '3306',
	  	database : 'db_cms',
	  	user     : 'db_root_dsv',
	  	password : 'Fio12345'
	}),


    getConnection: function(callback) {

    	try{

    		module.exports.pool.getConnection(function(err, connection) {
			  
			  if(err){
			  	console.log("Erro ao se conectar ao MySQL");
			  	console.log(err);
			  	throw err;
			  }else{

			  	callback(null,connection);

			  }

			});

    	}catch(e){
    		callback(e,null);
    	}

    },

    ping: function(conn){

    	conn.ping(function (err) {
		  if (err){
		  	console.log(err);
		  }else{
		  	console.log('Server responded to ping');
		  }

		});

    }

}