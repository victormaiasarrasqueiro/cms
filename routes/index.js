var express = require('express');
var router = express.Router();
var artigoFacade = require('./../facade/artigoFacade');
var categoriaFacade = require('./../facade/categoriaFacade');

/* GET home page. */
router.get('/', function(req, res, next) {

	var categorias = [];

	categoriaFacade.getListaCategoria(function(err,results){

		if(err){
			res.json(err);
		}else{

			//res.json(results);
			categorias = results;

			console.log(categorias);

			res.render('index', { 'title': 'Express', 'categorias':categorias });

		}

	});

});

/* GET home page. */
router.get('/app', function(req, res, next) {
  res.render('app', { title: 'Express' });
});

/* GET Artigo
router.get('/artigo/?', function(req, res, next) {
  res.render('artigo', { title: 'Express' });
});
 */

/* GET Artigo */
router.get('/artigo/:map', function(req, res, next) {

	
	artigoFacade.getArtigoPorPagina(req.params.map,function(err,results){

		if(err){
			res.json(err);
		}else{

			//res.json(results);
			res.render('artigo', { obj: results[0] });

		}

	});



});

module.exports = router;
