var express = require('express');

var router = express.Router();

var conn = require('./../conn');

var categoriaFacade = require('./../facade/categoriaFacade');


router.get('/', function(req, res, next) {

	
	categoriaFacade.getListaCategoria(function(err,results){

		if(err){
			res.json(err);
		}else{
			res.json(results);

		}

	});



});

module.exports = router;
