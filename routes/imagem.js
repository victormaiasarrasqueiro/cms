var express = require('express');

var router = express.Router();

var imagemFacade = require('./../facade/imagemFacade');


router.get('/', function(req, res) {

	
	imagemFacade.getListaImagem(function(err,results){

		if(err){
			res.json(err);
		}else{

			//res.json(results);
			res.json(results);

		}

	});

});

router.post('/', function(req, res) {

	var sucess = true;

	for(var i = 0; i<req.body.files.length; i++){

		var imagem = req.body.files[i];
		
		if( imagem.filetype != "image/jpeg" && imagem.filetype != "image/png" ){
			res.json( {erro:"Tipo de imagem invalido: " + imagem.filename } );
			sucess = false;
			break;
		}

		if( imagem.filesize > 250000 ){
			res.json( {erro:"Tamanho inválido: " + imagem.filename } );
			sucess = false;
			break;
		}

	}

	if(sucess){

		imagemFacade.inserirListaImagem(req.body.files,function(err,results){

			if(err){
				res.json(err);
			}else{

				console.log(results);
				res.json(results);

			}

		});

	};

});

module.exports = router;
