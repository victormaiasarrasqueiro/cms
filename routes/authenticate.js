var express       = require('express');
var jwt           = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config        = require('../config'); // get our config file
var router        = express.Router();

var usuarioFacade = require('./../facade/usuarioFacade');

var nodemailer = require('nodemailer');
var conta = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'victormaiasarrasqueiro@gmail.com', // Seu usuário no Gmail
        pass: 'et4sdA123' // A senha da sua conta no Gmail :-)
    }
});


// Route to authenticate a user (POST http://localhost:8080/api/authenticate)
router.post('/', function(req, res) {

  try{
    
    usuarioFacade.getUsuarioPorEmail(req.body.email,function(err,usuario){

      if(err){
        res.json(err);
      }else{

        if(usuario){

          if( usuario.senha === req.body.senha ){

            usuario.dt = new Date();
          
            var user = {};
            user.dt = new Date();
            user.nm = usuario.nm;
            user.id = usuario.id;
            user.email = usuario.email;


            // sign with RSA SHA256
            var key   = config.privateKey;          // get private key

            var token = jwt.sign(user, key, {
              expiresIn: '24h'                      
            });

            // return the information including token as JSON
            res.json({
              tp:1,
              success: true,
              code:200,
              message: 'Enjoy your token!',
              token: token
              
            });

          }else{
            res.json({success:false, errors:[{cod:600, message:"Senha inválida" }] });
          }

        }else{

          res.json({success:false, errors:[{cod:500, message:"Usuario inválido" }] });
        }

      };

    });

  }catch(e){
    console.log("Ocorreu um erro ao gerar o token.");
    throw e;
  }

});


// Route to authenticate a user (POST http://localhost:8080/api/authenticate)
router.post('/rememberme', function(req, res) {

  try{

    usuarioFacade.getUsuarioPorEmail(req.body.email,function(err,usuario){

      if(err){
        res.json(err);
      }else{

        if(usuario){

          conta.sendMail({
                         
             from: 'Suporte', 
             to:   usuario.email,
             subject: 'Envio de Senha', // O assunto
             html: '<strong>Olá!</strong><p> A sua senha de acesso é ' + usuario.senha + '</p>', // O HTMl do nosso e-mail
        
          }, 
          function(err){
            
            if(err){
              throw err;
            }

            res.json({success:true});

          });

        }else{

          res.json({success:false, errors:[{cod:500, message:"Usuario inválido" }] });
        }

      };

    });

  }catch(e){
    console.log("Ocorreu um erro ao gerar o token.");
    throw e;
  }

});

module.exports = router;
