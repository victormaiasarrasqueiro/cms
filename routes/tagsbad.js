var express = require('express');

var router = express.Router();

var conn = require('./../conn');


router.get('/', function(req, res, next) {

	try{

		conn.getConnection(function(err,conn){

		  	if(err){
				throw err;
		  	}else{


		  		var sql    = "SELECT * FROM tb_tags_bad";

				var query = conn.query(sql, function(err, results) {

					if(err){

						conn.release();
						throw err;
					}


					conn.release();
					
					res.json(results);
						
				});

			
			}//Fim do Else


		});


	}catch(e){

		if(conn){
  			
  			conn.release();
  		}

		res.json(e);

	}


});

/* GET users listing. */
router.post('/:text' , function(req, res) {

	try{


		conn.getConnection(function(err,conn){


		  	if(err){
				throw err;
		  	}else{


				conn.beginTransaction(function(err) {


					if(err){
						throw err;
					}

					
					conn.query('INSERT INTO tb_tags_bad SET ?', { text:req.params.text }, function(err, tag) {
	  
	  
						if (err) {
				      
					      return conn.rollback(function() {
					        throw err;
					      });
				    
					    }else{

							
							conn.commit(function(err) {
				        				
		        				if (err) {
						          
						          return conn.rollback(function() {
						            throw err;
						          });

						        }else{

						       
						        	if(conn){
							  			conn.release();
							  		}
						        	
						        	res.json('Incluido com sucesso!!!');

						        };
						        

					      	}); // Fim do Commit


						} // fim do else

	  
					}); // Fim do primeiro INSERT


				});

			}//Fim do Else


		});

	  	

  	}catch(e){

  		if(conn){
  			console.log("Erro ao inserir uma bad tag: " + e);
  			conn.release();
  		}

  		res.json(e);

  	}


});



module.exports = router;
