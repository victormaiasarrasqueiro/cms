var express = require('express');

var router = express.Router();

var conn = require('./../conn');
//var constants = require('./../constants');

var fs = require('fs');

var artigoFacade = require('./../facade/artigoFacade');
var imagemFacade = require('./../facade/imagemFacade');
var slackFacade  = require('./../facade/slackFacade');
var artigoTagsFacade = require('./../facade/artigoTagsFacade');

/* GET users listing. */
router.post('/' , function(req, res) {

	try{
		

		if(req.body.titulo && req.body.subtitulo && req.body.textHtml && req.body.url){


	  		conn.getConnection(function(err,conn){


			  	if(err){
					throw err;
			  	}else{


	  				conn.beginTransaction(function(err) {


	  					if(err){
	  						throw err;
	  					}


						imagemFacade.inserirImagem(req.body.img,function(err,imagemId){

					    		
				    		if(err){

				    			return conn.rollback(function() {
					            	throw err;
			          			});


				    		}else{


				    			var artigoTemp = { 

				    				titulo:req.body.titulo, 
				    				subtitulo:req.body.subtitulo, 
				    				textHtml:req.body.textHtml, 
				    				img:imagemId, 
				    				categoria:req.body.categoria,
				    				usuario: req.decoded.id,
				    				status: 1

				    			};

			    				conn.query('INSERT INTO tb_artigo SET ?', artigoTemp , function(err, artigo) {
	  
	  
									if (err) {
								      
								      return conn.rollback(function() {
								        throw err;
								      });
								    
								    }else{

									
										conn.query('INSERT INTO tb_pagina SET ?', { id_artigo:artigo.insertId, url:req.body.url.toLowerCase() }, function(err, pagina) {
							  
							  
											if (err) {
								      
										      return conn.rollback(function() {
										        throw err;
										      });
										    
										    }else{

											    conn.commit(function(err) {
					        				
							        				
							        				if (err) {
											          
											          return conn.rollback(function() {
											            throw err;
											          });

											        }else{

											       
											        	if(conn){
												  			conn.release();
												  		}

												  		console.log(req.body.tags);
												  		artigoTagsFacade.saveTagsArtigo(artigo.insertId,req.body.tags,function(err,resultado){

												  			console.log("--- s ss ----")
												  			console.log(resultado);
												  			console.log("--- s ss ----")

												  			//slackFacade.sendMessage(":card_index:  " + req.decoded.nm + "  escreveu um novo artigo: '" + req.body.titulo + "'.");
								  		
											    			res.json({artigo:artigo.insertId,pagina:pagina.insertId,imagem:imagemId});

												  		});


											        };
											        

										      	}); // Fim do Commit

											} // Fim do Else

							  
										}); // Fim do segundo INSERT


									} // fim do else

					  
								}); // Fim do primeiro INSERT
					    			

				    		};

				    	
				    	});



	  				}); // fim do begin transaction

	  			}//Fim do Else


	  		});

	  	}else{

	  		res.json("Falha na validação do formulario");
	  	}

  	}catch(e){

  		if(conn){
  			console.log("Erro ao inserir um artigo: " + e);
  			conn.release();
  		}

  		res.json(e);

  	}

});


router.post('/verificarURL', function(req, res) {
	
	artigoFacade.getArtigo(req.body.url, function(err,results){

		if(err){
			res.json(err);
		}else{

			if(results != null && results.length  > 0){
				res.json({result:false});
			}else{
				res.json({result:true});
			}
		}

	});

});


router.get('/', function(req, res) {

	artigoFacade.getListaArtigoPorUsuario(req.decoded.id, function(err,results){

		if(err){
			res.json(err);
		}else{

			
			res.json(results);
			
		}

	});

});

router.get('/:idArtigo', function(req, res) {

	artigoFacade.getArtigo(req.params.idArtigo,  function(err,artigo){

		if(err){
			res.json(err);
		}else{

			res.json(artigo);
			
		}

	});

});

router.get('/all', function(req, res) {

	artigoFacade.getListaArtigo(function(err,results){

		if(err){
			res.json(err);
		}else{

			
			res.json(results);
			
		}

	});

});



module.exports = router;
