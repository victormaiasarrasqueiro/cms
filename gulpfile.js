var gulp            = require('gulp');
var angularFilesort = require('gulp-angular-filesort');
var inject          = require('gulp-inject');
var jsmin 			= require('gulp-jsmin');
var rename 			= require('gulp-rename');
var concat          = require('gulp-concat'); 
var uglify = require('gulp-uglify');
var merge = require('merge-stream');


gulp.task('default', function() {


//LIBS
var js_libs = 
gulp.src([	'./public/bower_components/jquery/dist/jquery.min.js',
			'./public/bower_components/angular/angular.min.js',
			'./public/bower_components/angular-animate/angular-animate.min.js',
			'./public/bower_components/angular-aria/angular-aria.min.js',
			'./public/bower_components/angular-messages/angular-messages.min.js',
			'./public/bower_components/bootstrap/dist/js/bootstrap.min.js',
			'./public/bower_components/angular-locale-pt-br/angular-locale_pt-br.js',
			'./public/bower_components/angular-ui-router/release/angular-ui-router.min.js',
			'./public/bower_components/textAngular/dist/textAngular-rangy.min.js',
			'./public/bower_components/textAngular/dist/textAngular-sanitize.min.js',
			'./public/bower_components/textAngular/dist/textAngular.min.js',
			'./public/bower_components/toastr/build/toastr.min.js',
			'./public/bower_components/angular-base64-upload/dist/angular-base64-upload.min.js',
			'./public/bower_components/ng-tags-input/ng-tags-input.min.js',
			'./public/bower_components/ngstorage/ngStorage.min.js',
			'./public/bower_components/datatables.net/js/jquery.dataTables.min.js',
			'./public/bower_components/angular-datatables/dist/angular-datatables.min.js',
			'./public/bower_components/spin.js/spin.min.js',
			'./public/bower_components/angular-spinner/angular-spinner.min.js',
			'./public/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'
		])
.pipe(concat('app-libs.js'))
.pipe(gulp.dest('./public/build'));


// SISTEMA
var js_sistema = 

gulp.src(['./public/app/**/*.js'])
.pipe(angularFilesort())
.pipe(concat('app-sistema.js'))
.pipe(gulp.dest('./public/build'))




// Criando o APP.JS
merge(js_libs, js_sistema)
.pipe(concat('app.js'))
.pipe(uglify({ mangle: false }))
.pipe(gulp.dest('./public/build'))

});