(function() {
    'use strict';

    angular.module('app', [
		'app.directives',
        'app.core',
        'app.categoria',
        'app.curso',
        'app.usuario',
		'app.artigo',
        'app.imagem',
		'app.login'
        
    ]);


    angular.module('app').config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });

    angular.module('app').factory('AuthInterceptor', function ($rootScope, $q, $localStorage,$location) {
       
        'use strict';
        
        return {

            request: montarCabecalhoReq,
            response: recuperarCabecalhoRes

        };

        function montarCabecalhoReq(config) {

            config.headers = config.headers || {};
            config.headers['Content-Type'] = 'application/json';
            
            var token = $localStorage.token;

            if (token){
                config.headers.Authorization = 'Bearer ' + token;
            }

            return config;
        }

        function recuperarCabecalhoRes(response){

            var deferred = $q.defer();

            if( response.data.code && ( response.data.code == 401 || response.data.code == 403) ){
             
                $location.path('/login');
            
            }

            deferred.resolve(response);
            
            return deferred.promise;

        }

    });


})();