﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.usuario').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.usuarioatualizar',
				config: {
					url: '/usuario/atualizar',
					title: 'Atualizar Usuário',
					views: {
						'content@app': {
							templateUrl: 'usuario/usuario.atualizar.html',
							controller: 'UsuarioAtualizarController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.usuariolistar',
				config: {
					url: '/usuario/listar',
					title: 'Listar Usuários',
					views: {
						'content@app': {
							templateUrl: 'usuario/usuario.listar.html',
							controller: 'UsuarioListarController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();
