﻿(function() {
    'use strict';

    angular.module('app.usuario').controller('UsuarioListarController', UsuarioListarController);

	UsuarioListarController.$inject = ['$scope','$http'];

    function UsuarioListarController($scope,$http) {
        
		var vm = this;

		vm.enviarConvite = enviarConvite;

		vm.open = false;
		vm.lista = [];
	
		vm.obj = {};

		/*
		* Função Inicio.
		*/
		function activate() {
			
			$http.get('/usuario', {}).then(function(res){

				vm.lista = res.data;

			});
						
        };

        function enviarConvite(){

       	 	$http.post('/usuario', angular.toJson(vm.obj), {}).then(function(){

       	 		
       	 	});


        }
		

		// Iniciando o procedimento.
		activate();
		
	};

})();

