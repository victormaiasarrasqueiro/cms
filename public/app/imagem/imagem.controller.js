﻿(function() {
    'use strict';

    angular.module('app.categoria').controller('ImagemController', ImagemController);

	ImagemController.$inject = ['$scope','$http','usSpinnerService','$uibModal'];

    function ImagemController($scope,$http,usSpinnerService,$uibModal) {
        
		var vm = this;
		vm.enviar = enviar;
		vm.visualizarImagem = visualizarImagem;

		vm.obj = {};
		
		vm.lista = [];

		/*
		* Função Inicio.
		*/
		function activate() {
			
			listarImagens();
						
        };

        function listarImagens(){

        	$http.get('/imagem', {} ).then(function(res){

				vm.lista = res.data;

			});

        };

        function enviar(){

        	startSpin();

        	$http.post('/imagem', angular.toJson(vm.obj), {} ).then(function(res){

       	 		stopSpin();

       	 		if(res.data.erro){
       	 			toastr.error(res.data.erro, 'Ocorreu um erro!')
       	 		}else{
       	 			toastr.success('Suas imagens foram inseridas.', 'Tudo certo!')
       	 		}

       	 	});

        }
		
		function visualizarImagem(id){

			var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'imagem/visualizar-imagem/visualizar.imagem.html',
		      	controller: 'VisualizarImagemController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	id: function () {

			        	return id;

		        	}

		      	}
		    });	


     		modalInstance.result.then(function () {
	      		
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

		}

		function startSpin() {

	        usSpinnerService.spin('spinner-1');
	       
	    };

	    function stopSpin() {
	      
	        usSpinnerService.stop('spinner-1');
	      
	    };

		// Iniciando o procedimento.
		activate();
		
	};

})();

