﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.imagem').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.imagem',
				config: {
					url: '/imagens',
					title: 'Imagens',
					views: {
						'content@app': {
							templateUrl: 'imagem/imagem.html',
							controller: 'ImagemController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();
