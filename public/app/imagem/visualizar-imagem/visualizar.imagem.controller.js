﻿(function() {
    'use strict';

    angular.module('app.artigo').controller('VisualizarImagemController', VisualizarImagemController);

	VisualizarImagemController.$inject = ['$scope','$http','$log','$uibModalInstance','id'];

    function VisualizarImagemController($scope,$http,$log,$uibModalInstance,id) {
        
		var vm = this;
		vm.finalizar = finalizar;
		vm.cancelar = cancelar;

		vm.obj = {};

		vm.obj.id = id;



		function finalizar(){

			$uibModalInstance.close(vm.obj.tags);

		}

		function cancelar(){
			$uibModalInstance.dismiss('cancel');
		}


		
	};

})();

