﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.curso').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.curso',
				config: {
					url: '/cursos',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'app/curso/curso.html',
							controller: 'CursoController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();
