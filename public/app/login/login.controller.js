﻿(function() {
    'use strict';

    angular.module('app.login').controller('LoginController', LoginController);

	LoginController.$inject = ['$scope','$http','$localStorage','$state','$rootScope', '$q'];

    function LoginController($scope,$http,$localStorage,$state,$rootScope, $q) {
        
		var vm = this;
		vm.entrar = entrar;

		vm.obj = {};
		vm.obj.email = "";
		vm.obj.senha = "";
		vm.obj.esqueci = false;

		/*
		* Função Inicio.
		*/
		function activate() {
			
						
        };

        function entrar(){

        	if(vm.obj.esqueci){

        		if(vm.obj.email){
        			esqueci();
        		}else{
        			alert("Digite o e-mail");
        		}

        	}else{

        		if(vm.obj.email && vm.obj.senha){
        			login();
        		}else{
        			alert("Digite as informações para entrar");
        		}
        		
        	}
        }

        function esqueci(){

        	$http.post('/authenticate/rememberme', angular.toJson(vm.obj), {}).then(function(res){

       	 		if(res.data.success){
       	 			alert("Email enviado!");
       	 		}else{

       	 			alert(res.data.errors[0].message);
       	 		}

       	 	});

        }

        function login(){

        	$http.post('/authenticate', angular.toJson(vm.obj), {}).then(function(res){

       	 		if(res.data.token){
       	 			$localStorage.token = res.data.token;
              $localStorage.dateL = new Date();
       	 			$state.transitionTo("app.artigo", {} );
       	 		}else{

       	 			alert(res.data.errors[0].message);
       	 		}

       	 	});

        }
		
		// Iniciando o procedimento.
		activate();
		
	};

})();

