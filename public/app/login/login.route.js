﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.login').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.login',
				config: {
					url: '/',
					title: 'Login',
					views: {
						'content@app': {
							templateUrl: 'login/login.html',
							controller: 'LoginController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.login2',
				config: {
					url: '/login',
					title: 'Login',
					views: {
						'content@app': {
							templateUrl: 'login/login.html',
							controller: 'LoginController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();
