(function() {
    'use strict';

    angular.module('app.core', [
        'ui.router',
		'blocks.router',
		'ngAnimate',
	
		'textAngular',
		'naif.base64',
		'angularSpinner',
		'datatables',
		'ngStorage',
		'ngTagsInput',
		'ui.bootstrap'
    ]);


    

})();
