﻿(function() {
    'use strict';

    angular.module('app.artigo').controller('GeradorTagsController', GeradorTagsController);

	GeradorTagsController.$inject = ['$scope','$http','$log','$uibModalInstance','tags','tagsbad'];

    function GeradorTagsController($scope,$http,$log,$uibModalInstance,tags,tagsbad) {
        
		var vm = this;
		vm.clear = clear;
		vm.go = go;
		vm.finalizar = finalizar;
		vm.cancelar = cancelar;

		vm.tags = false;
		vm.comoBuscarPt = false;
		vm.comoBuscarEng = false;
		vm.anunciosPt = false;
		vm.anunciosEng = false;
		vm.tagsRevisao = false;

		vm.obj = {};
		vm.obj.tags = angular.copy(tags);
		vm.obj.tagsbad = angular.copy(tagsbad);

		vm.atual = 1;
		vm.anterior = 0;
		vm.proximo = 2;

		vm.obj.campo1 = ""; 
		vm.obj.campo2 = ""; 
		vm.obj.campo3 = ""; 
		vm.obj.campo4 = ""; 
		vm.obj.campo5 = ""; 
		vm.obj.campo6 = ""; 
		vm.obj.campo7 = ""; 
		vm.obj.campo8 = ""; 
		vm.obj.campo9 = ""; 
	    vm.obj.campo10 = "";

		function inicio(){

			go(1);
			clear();

		}

		function go(p){

			closeAll();

			if(p == 1){
				
				pagina1();

			}else if(p == 2){

				pagina2();

			}else if(p == 3){

				pagina3();

			}else if(p == 4){

				pagina4();

			}else if(p == 5){

				pagina5();

			}else if(p == 6){

				pagina6();

			}

		}

		function pagina1(){
			vm.tags = true;
			setPages(1,0,2);
		}

		function pagina2(p) {
			vm.comoBuscarPt = true;
			setPages(2,1,3);
		}

		function pagina3(p) {
			vm.comoBuscarEng = true;
			setPages(3,2,4);
		}

		function pagina4(p) {
			vm.anunciosPt = true;
			setPages(4,3,5);
		}

		function pagina5(p) {
			vm.anunciosEng = true;
			setPages(5,4,6);
		}

		function pagina6(p) {

			var uniao = vm.obj.campo1.trim() + " " + vm.obj.campo2.trim() + " " + vm.obj.campo3.trim() + " " + vm.obj.campo4.trim() + " " + vm.obj.campo5.trim() + " " + vm.obj.campo6.trim() + " " +  vm.obj.campo7.trim() + " " + vm.obj.campo8.trim() + " " + vm.obj.campo9.trim() + " " + vm.obj.campo10.trim();

			var arrayUniao = uniao.split(" ");

			arrayUniao = arrayUniao.map(function(element) {
    			var s = element.trim();

			  return {text:s};
			});

			vm.obj.tags = angular.copy(vm.obj.tags.concat(arrayUniao));

			clear();

			vm.tagsRevisao = true;
			setPages(6,5,10);
		}
		
		function setPages(atual,anterior,proxima){
			vm.atual = atual;
			vm.anterior = anterior;
			vm.proximo = proxima;
		}

		function closeAll(){
			vm.tags = false;
			vm.comoBuscarPt = false;
			vm.comoBuscarEng = false;
			vm.anunciosPt = false;
			vm.anunciosEng = false;
			vm.tagsRevisao = false;
		}

		function clear(){


			removerBrancos();
			
			removerDuplicidades();

			removerTodosTagsBad();
			
		}

		function removerTodosTagsBad(){

			var temp = angular.copy(vm.obj.tags);

			for(var i = 0 ; i < vm.obj.tagsbad.length ; i++){

				temp = removerItemLista(temp,vm.obj.tagsbad[i]);
			}

			vm.obj.tags = temp;

		};

		function removerItemLista(lista,item){

			for(var i = 0 ; i < lista.length ; i++){

				if( lista[i].text.toUpperCase() == item.text.toUpperCase()  ){
					lista.splice(i, 1);
					break;
				}

			}

			return lista;

		};

		function removerDuplicidades(){

			var temp = [];

			for(var i = 0 ; i < vm.obj.tags.length ; i++){

				if(!foiIncluido(temp, vm.obj.tags[i])){
					temp.push(angular.copy(vm.obj.tags[i]));
				}
				
			}

			vm.obj.tags = temp;

		}

		function foiIncluido(lista,termo){

			var retorno = false;
			
			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.toUpperCase() == termo.text.toUpperCase()){
					retorno = true;
					break;
				}
				
			}

			return retorno;

		}

		function removerBrancos(){

			var temp = [];

			for(var i = 0 ; i < vm.obj.tags.length ; i++){

				var p = vm.obj.tags[i].text.trim();

				if(p.length > 2 && p.length < 12){

					temp.push({text:p});
				}
				
			}

			vm.obj.tags = temp;

		}

		function finalizar(){

			$uibModalInstance.close(vm.obj.tags);

		}

		function cancelar(){
			$uibModalInstance.dismiss('cancel');
		}

		closeAll();
		inicio();
	};

})();

