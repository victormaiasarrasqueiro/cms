﻿(function() {
    'use strict';

    angular.module('app.artigo').controller('ArtigoController', ArtigoController);

	ArtigoController.$inject = ['$scope','$http','$log','$uibModal','$state', 'lista'];

    function ArtigoController($scope,$http,$log,$uibModal,$state,lista) {
        
		var vm = this;
		vm.edit = edit;

		vm.artigos = [];

		function activate(){

			vm.artigos = angular.copy(lista);

		}

		function edit(idC){

			$state.transitionTo("app.editarartigo", { 'id':idC} );
		};

		// Iniciando o procedimento.
		activate();
		
	};

})();

