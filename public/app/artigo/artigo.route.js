﻿/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.artigo').run(appRun);

    appRun.$inject = ['routerHelper','$stateParams','$http'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.artigo',
				config: {
					url: '/artigos',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/artigo.html',
							controller: 'ArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						lista: function($stateParams,$http){

							return $http.get('/artigos').then(function(res){
							
								return res.data;

							}); 
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.incluirartigo',
				config: {
					url: '/artigos/incluir',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/incluir.artigo.html',
							controller: 'IncluirArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						artigo: function($stateParams){

							return null; 
							
						},
						categorias: function($http){

							return $http.get('/categoria', {}).then(function(res){

								return res.data;

							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{	state: 'app.editarartigo',
				config: {
					url: '/artigos/editar/:id',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/incluir.artigo.html',
							controller: 'IncluirArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						artigo: function($stateParams,$http){

							var url = '/artigos/' + $stateParams.id;
							return $http.get(url).then(function(res){
							
								return res.data;

							}); 
							
						},
						categorias: function($http){

							return $http.get('/categoria', {}).then(function(res){

								return res.data;

							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();
