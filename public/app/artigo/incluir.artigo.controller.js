﻿(function() {
    'use strict';

    angular.module('app.artigo').controller('IncluirArtigoController', IncluirArtigoController);

	IncluirArtigoController.$inject = ['$scope','$http','$log','$uibModal','artigo','categorias'];

    function IncluirArtigoController($scope,$http,$log,$uibModal,artigo,categorias) {
        
		var vm = this;
		vm.incluir = incluir;
		vm.sugerir = sugerir;
		vm.abrirGeradorTags = abrirGeradorTags;
		vm.abrirCheckListQualidade = abrirCheckListQualidade;


		vm.isCollapsedTitulo = false;
		vm.isCollapsedSubTitulo = false;

		vm.taOptions = [
		      ['html'],['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'pre' ],
		      ['bold', 'italics', 'underline', 'ul', 'ol'],
		      ['justifyCenter', , 'indent', 'insertImage','insertLink', 'insertVideo', 'charcount'],
	  	];

		vm.obj = {};
		vm.obj.titulo = "";
		vm.obj.subtitulo = "";
		vm.obj.textHtml = "";
		vm.obj.url =  "";
		vm.obj.categoria = 0;
		vm.obj.tags = [];
		vm.obj.images = null;

		vm.categorias = [];


        vm.validations = {
        	url:false
        }

        vm.conteudoInicial = '';
		vm.conteudoInicial = vm.conteudoInicial + '<h3>​Titulo de Sessão - usando &lt;h3&gt;</h3>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>O conteúdo deverá ser dividido em parágrafos e cada um destes escrito utilizando a tag &lt;p&gt;. Para os textos é importante sempre usar a tag &lt;p&gt;. É importante respeitar a ortografia. Os autores estão livres para utilizar vídeos do YouTube, imagens externas ou links para outras páginas.</p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>É necessário que todas as<b> palavras ou trechos importantes do conteúdo estejam em negrito </b><u>ou subinhad</u><u>o.</u></p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p style="text-align: center;"><i>Tente evitar muitos espaços​ ou itens centralizados!</i></p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>Códigos precisam estar entre as tags &lt;pre&gt; e &lt;code&gt; e especificando a linguagem usada. Exemplo:</p>';

vm.conteudoInicial = vm.conteudoInicial + '<pre><code class="language-java">';
vm.conteudoInicial = vm.conteudoInicial + '\n';
vm.conteudoInicial = vm.conteudoInicial + 'public class Blog{	\n';	
vm.conteudoInicial = vm.conteudoInicial + '	private String teste;	\n';	
vm.conteudoInicial = vm.conteudoInicial + '	public String getTeste(){	\n';		
vm.conteudoInicial = vm.conteudoInicial + '		return teste;		\n';
vm.conteudoInicial = vm.conteudoInicial + '	}	\n';
vm.conteudoInicial = vm.conteudoInicial + '}\n';
vm.conteudoInicial = vm.conteudoInicial + '\n';
vm.conteudoInicial = vm.conteudoInicial + '</code>​</pre>\n';



		/*
		* Função Inicio.
		*/
		function activate() {
			
			vm.categorias = angular.copy(categorias);

			vm.obj.textHtml = vm.conteudoInicial;

        };
		
		/*
		* Incluindo novo Artigo no Sistema
		*/
        function incluir(){
        	
        	vm.obj.img = angular.copy(vm.obj.imgTmp);
        	
       	 	$http.post('/artigos/', angular.toJson(vm.obj), {}).then(function(){
       	 		toastr.info('Inserido com sucesso!');	
       	 	});

        };

 
        /*
		* Sugerindo uma nova URL para o artigo.
		*/
        function sugerir(){
        	
        	if(vm.obj.titulo.length > 10){

	        	var titulo = angular.copy(vm.obj.titulo);
	        	var pos = titulo.indexOf(" ");
			    
			    while (pos > -1){
					titulo = titulo.replace(" ", "-");
					pos = titulo.indexOf(" ");
				}
			    
				vm.obj.url = titulo.toLowerCase();
        	}else{
        		toastr.info('Titulo do artigo esta muito pequeno');
        	}
        	
        }

        /*
		* Abrir a aplicação de Geracao de tags
		*/
        function abrirGeradorTags(){
 
		   	var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'artigo/geradorTags/gerador.tags.html',
		      	controller: 'GeradorTagsController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	tags: function () {

		        		if(!vm.obj.tags || vm.obj.tags.length == 0){

			        		var arrayTitulo = vm.obj.titulo.trim().split(" ");

			        		arrayTitulo = arrayTitulo.map(function(element){
			        			var s = element.trim();

							  return {text:s};
							});

			        		var arraySubTitulo = vm.obj.subtitulo.trim().split(" ");

			        		arraySubTitulo = arraySubTitulo.map(function(element) {
			        			var s = element.trim();

							  return {text:s};
							});

			        		var array = arrayTitulo.concat(arraySubTitulo);


			        		array = removerBrancos(array);


			          		return removerDuplicidades(array);

		        		}else{

		        			return vm.obj.tags;

		        		}
		        		
		        	},

		        	tagsbad: function(){

		        		return $http.get('/tagsbad', {}).then(function(res){

							return res.data;

						});

		        	}

		      	}
		    });	


     		modalInstance.result.then(function (tags) {
	      		vm.obj.tags = tags;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

        }


        /*
		* Abrir o modal de qualidade.
		*/
        function abrirCheckListQualidade(){
 
		   	var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'artigo/checkListQualidade/checklist.html',
		      	controller: 'CheckListQualidadeController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	url: function () {

		        		if(vm.obj.url.length > 50){
			        		
			        		return {success:false,msg:"A URL esta muito grande."};

			        	}else{
				       	 	
				       	 	$http.post('/artigos/verificarURL', angular.toJson({url:vm.obj.url}), {}).then(function(res){
				       	 		return {success:res.data.result,msg:"URL existente."};
				       	 	});

			       	 	}
		        		
		        	},

		        	artigo: function(){

						return vm.obj;

		        	}

		      	}
		    });	


     		modalInstance.result.then(function (tags) {
	      		vm.obj.tags = tags;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

        }


        /*
		* Funcoes auxiliares
		*/
        function removerDuplicidades(lista){

			var temp = [];

			for(var i = 0 ; i < lista.length ; i++){

				if(!foiIncluido( temp, lista[i] )){
					temp.push(angular.copy(lista[i]));
				}
				
			}

			return temp;

		}
		function removerBrancos(lista){

			var temp = [];

			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.trim().length > 0){
					temp.push(angular.copy(lista[i]));
				}
				
			}

			return temp;

		}
		function foiIncluido(lista,termo){

			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.toUpperCase() == termo.text.toUpperCase()){
					return true;
				}
				
			}

			return false;

		}






		// Iniciando o procedimento.
		activate();
		





	};

})();

