(function() {
    'use strict';

    angular.module('app.artigo', []);

})();

(function() {
    'use strict';

    angular.module('app.artigo').controller('VisualizarImagemController', VisualizarImagemController);

	VisualizarImagemController.$inject = ['$scope','$http','$log','$uibModalInstance','id'];

    function VisualizarImagemController($scope,$http,$log,$uibModalInstance,id) {
        
		var vm = this;
		vm.finalizar = finalizar;
		vm.cancelar = cancelar;

		vm.obj = {};

		vm.obj.id = id;



		function finalizar(){

			$uibModalInstance.close(vm.obj.tags);

		}

		function cancelar(){
			$uibModalInstance.dismiss('cancel');
		}


		
	};

})();


(function() {
    'use strict';

  angular.module('sidebar.directive', []);

	angular.module('sidebar.directive').directive('sidebar',['$location',function() {
    return {
      templateUrl:'app/directives/sidebar/sidebar.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope){

        $scope.selectedMenu = 'dashboard';
        $scope.collapseVar = 0;
        $scope.multiCollapseVar = 0;
        
        $scope.check = function(x){
          
          if(x==$scope.collapseVar)
            $scope.collapseVar = 0;
          else
            $scope.collapseVar = x;
        };
        
        $scope.multiCheck = function(y){
          
          if(y==$scope.multiCollapseVar)
            $scope.multiCollapseVar = 0;
          else
            $scope.multiCollapseVar = y;
        };
		    
      }
    }
  }]);

})();
(function() {
  'use strict';

  angular.module('blocks.router', [
    'ui.router',
    'blocks.logger'
  ]);

})();
(function() {
  'use strict';

  angular.module('blocks.router').provider('routerHelper', routerHelperProvider);

  // routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
  routerHelperProvider.$inject = ['$stateProvider', '$urlRouterProvider'];

  // function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
  function routerHelperProvider($stateProvider, $urlRouterProvider) {

    var config = {
      docTitle: 'WDEV-POC', //automatizar pelo Gulp
      resolveAlways: {}
    };

    // $locationProvider.html5Mode(true);

    this.configure = function(cfg) {
      angular.extend(config, cfg);
    };

    this.$get = RouterHelper;
    RouterHelper.$inject = ['$location', '$rootScope', '$state', 'logger','$localStorage'];

    function RouterHelper($location, $rootScope, $state, logger,$localStorage) {
      var handlingStateChangeError = false;
      var hasOtherwise = false;
      var stateCounts = {
        errors: 0,
        changes: 0
      };

      var service = {
        configureStates: configureStates,
        getStates: getStates,
        stateCounts: stateCounts
      };

      init();

      return service;


      function configureStates(states, otherwisePath) {
        states.forEach(function(state) {
          state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
          $stateProvider.state(state.state, state.config);
        });
        if (otherwisePath && !hasOtherwise) {
          hasOtherwise = true;
          $urlRouterProvider.otherwise(otherwisePath);
        }
      }

      function handleRoutingErrors() {
        // Route cancellation:
        // On routing error, go to the dashboard.
        // Provide an exit clause if it tries to do it twice.
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
          if (handlingStateChangeError) {
            return;
          }
          stateCounts.errors++;
          handlingStateChangeError = true;
          var destination = (toState && (toState.title || toState.name || toState.loadedTemplateUrl)) || 'unknown target';
          var msg = 'Error routing to ' + destination + '. ' + (error.data || '') + '. <br/>' + (error.statusText || '') + ': ' + (error.status || '');
          logger.warning(msg, [toState]);
          $location.path('/');
        });
      }

      function init() {
        handleRoutingErrors();
        updateDocTitle();
      }

      function getStates() {


        return $state.get();
      }

      function updateDocTitle() {

        $rootScope.$on('$stateChangeSuccess',
          function(event, toState, toParams, fromState, fromParams) {


          
            if(toState.name && toState.name != "app.login" && toState.name != "app.login2"){

                if(!$localStorage.token || !$localStorage.dateL){
                    $location.path('/login');
                }
         
            }

            stateCounts.changes++;
            handlingStateChangeError = false;
            var title = config.docTitle + ' ' + (toState.title || '');
            $rootScope.title = title;
          }
        );
      }
    }

  }

})();
(function() {
  'use strict';

  angular.module('blocks.permission', ['ui.router']).run(appRun);

  appRun.$inject = ['$rootScope', 'Permission', '$state'];

  function appRun($rootScope, Permission, $state) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      // If there are permissions set then prevent default and attempt to authorize
      var permissions;
      if (toState.data && toState.data.permissions) {
        permissions = toState.data.permissions;
      } else if (toState.permissions) {
        /**
        * This way of defining permissions will be depracated in v1. Should use
        * `data` key instead
        */
        console.log('Deprecation Warning: permissions should be set inside the `data` key ');
        console.log('Setting permissions for a state outside `data` will be depracated in version 1');
        permissions = toState.permissions;
      }

      if (permissions) {
        event.preventDefault();
        Permission.authorize(permissions, toParams).then(function () {
          // If authorized, use call state.go without triggering the event.
          // Then trigger $stateChangeSuccess manually to resume the rest of the process
          // Note: This is a pseudo-hacky fix which should be fixed in future ui-router versions
          if (!$rootScope.$broadcast('$stateChangeStart', toState.name, toParams, fromState.name, fromParams).defaultPrevented) {
            $rootScope.$broadcast('$stateChangePermissionAccepted', toState, toParams);
            $state.go(toState.name, toParams, {notify: false}).then(function() {
              $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
            });
          }
        }, function () {
          if (!$rootScope.$broadcast('$stateChangeStart', toState.name, toParams, fromState.name, fromParams).defaultPrevented) {
            $rootScope.$broadcast('$stateChangePermissionDenied', toState, toParams);
            // If not authorized, redirect to wherever the route has defined, if defined at all
            var redirectTo = permissions.redirectTo;
            if (redirectTo) {
              $state.go(redirectTo, toParams, {notify: false}).then(function() {
                $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
              });
            }

          }
        });
      }

    });

  }

})();

(function() {
  'use strict';

  angular.module('blocks.permission').provider('Permission', Permission);

  function Permission() {

    var roleValidationConfig = {};
    var validateRoleDefinitionParams = function (roleName, validationFunction) {
      if (!angular.isString(roleName)) {
        throw new Error('Role name must be a string');
      }
      if (!angular.isFunction(validationFunction)) {
        throw new Error('Validation function not provided correctly');
      }
    };

    this.defineRole = function (roleName, validationFunction) {
      /**
        This method is only available in config-time, and cannot access services, as they are
        not yet injected anywere which makes this kinda useless.
        Should remove if we cannot find a use for it.
      **/
      validateRoleDefinitionParams(roleName, validationFunction);
      roleValidationConfig[roleName] = validationFunction;

      return this;
    };

    this.$get = ['$q', function ($q) {
      var Permission = {
        _promiseify: function (value) {
          /**
            Converts a value into a promise, if the value is truthy it resolves it, otherwise
            it rejects it
          **/
          if (value && angular.isFunction(value.then)) {
            return value;
          }

          var deferred = $q.defer();
          if (value) {
            deferred.resolve();
          } else {
            deferred.reject();
          }
          return deferred.promise;
        },
        _validateRoleMap: function (roleMap) {
          if (typeof(roleMap) !== 'object' || roleMap instanceof Array) {
            throw new Error('Role map has to be an object');
          }
          if (roleMap.only === undefined && roleMap.except === undefined) {
            throw new Error('Either "only" or "except" keys must me defined');
          }
          if (roleMap.only) {
            if (!(roleMap.only instanceof Array)) {
              throw new Error('Array of roles expected');
            }
          } else if (roleMap.except) {
            if (!(roleMap.except instanceof Array)) {
              throw new Error('Array of roles expected');
            }
          }
        },
        _findMatchingRole: function (rolesArray, toParams) {
          var roles = angular.copy(rolesArray);
          var deferred = $q.defer();
          var currentRole = roles.shift();

          // If no roles left to validate reject promise
          if (!currentRole) {
            deferred.reject();
            return deferred.promise;
          }
          // Validate role definition exists
          if (!angular.isFunction(Permission.roleValidations[currentRole])) {
            throw new Error('undefined role or invalid role validation');
          }

          var validatingRole = Permission.roleValidations[currentRole](toParams);
          validatingRole = Permission._promiseify(validatingRole);

          validatingRole.then(function () {
            deferred.resolve();
          }, function () {
            Permission._findMatchingRole(roles, toParams).then(function () {
              deferred.resolve();
            }, function () {
              deferred.reject();
            });
          });

          return deferred.promise;
        },
        defineRole: function (roleName, validationFunction) {
          /**
            Service-available version of defineRole, the callback passed here lives in the
            scope where it is defined and therefore can interact with other modules
          **/
          validateRoleDefinitionParams(roleName, validationFunction);
          roleValidationConfig[roleName] = validationFunction;

          return Permission;
        },
        resolveIfMatch: function (rolesArray, toParams) {
          var roles = angular.copy(rolesArray);
          var deferred = $q.defer();
          Permission._findMatchingRole(roles, toParams).then(function () {
            // Found role match
            deferred.resolve();
          }, function () {
            // No match
            deferred.reject();
          });
          return deferred.promise;
        },
        rejectIfMatch: function (roles, toParams) {
          var deferred = $q.defer();
          Permission._findMatchingRole(roles, toParams).then(function () {
            // Role found
            deferred.reject();
          }, function () {
            // Role not found
            deferred.resolve();
          });
          return deferred.promise;
        },
        roleValidations: roleValidationConfig,
        authorize: function (roleMap, toParams) {
          // Validate input
          Permission._validateRoleMap(roleMap);

          var authorizing;

          if (roleMap.only) {
            authorizing = Permission.resolveIfMatch(roleMap.only, toParams);
          } else {
            authorizing = Permission.rejectIfMatch(roleMap.except, toParams);
          }

          return authorizing;
        }
      };

      return Permission;
    }];

  }

})();

(function() {
  'use strict';

  angular.module('blocks.logger', []);

})();

(function() {
  'use strict';

  angular.module('blocks.logger').factory('logger', logger);

  //logger.$inject = ['$log', 'toastr'];
  logger.$inject = ['$log'];

  //function logger($log, toastr) {
  function logger($log) {

    var service = {
      showToasts: true,
      error: error,
      info: info,
      success: success,
      warning: warning,
      // straight to console; bypass toastr
      log: $log.log
    };

    return service;

    ////////////////////////////////

    function error(message, data, title) {
      //toastr.error(message, title);
      $log.error('Error: ' + message, data);
    }

    function info(message, data, title) {
      //toastr.info(message, title);
      $log.info('Info: ' + message, data);
    }

    function success(message, data, title) {
      //toastr.success(message, title);
      $log.info('Success: ' + message, data);
    }

    function warning(message, data, title) {
      //toastr.warning(message, title);
      $log.warn('Warning: ' + message, data);
    }

  }

})();

(function() {
  'use strict';

  angular.module('blocks.exception', ['blocks.logger']);

})();

(function() {
  'use strict';

  angular.module('blocks.exception').factory('exception', exception);

  exception.$inject = ['logger'];

  function exception(logger) {
    var service = {
      catcher: catcher
    };

    return service;

    ///////////////////////////

    function catcher(message) {
      return function(reason) {
        logger.error(message, reason);
      };
    }
  }

})();

(function() {
  'use strict';

  angular.module('blocks.exception')
    .provider('exceptionHandler', exceptionHandlerProvider)
    .config(config);

  /**
   * Must configure the exception handling
   * @return {[type]}
   */
  function exceptionHandlerProvider() {
    this.config = {
      appErrorPrefix: '[Error] '
    };

    this.configure = function (appErrorPrefix) {
      this.config.appErrorPrefix = appErrorPrefix;
    };

    this.$get = function() {
      return {config: this.config};
    };
  }

  config.$inject = ['$provide'];

  /**
   * Configure by setting an optional string value for appErrorPrefix.
   * Accessible via config.appErrorPrefix (via config value).
   * @param  {[type]} $provide
   * @return {[type]}
   * @ngInject
   */
  function config($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
  }

  extendExceptionHandler.$inject = ['$delegate', 'exceptionHandler', 'logger'];

  /**
   * Extend the $exceptionHandler service to also display a toast.
   * @param  {Object} $delegate
   * @param  {Object} exceptionHandler
   * @param  {Object} logger
   * @return {Function} the decorated $exceptionHandler service
   */
  function extendExceptionHandler($delegate, exceptionHandler, logger) {
    return function(exception, cause) {
      var appErrorPrefix = exceptionHandler.config.appErrorPrefix || '';
      var errorData = {exception: exception, cause: cause};
      exception.message = appErrorPrefix + exception.message;
      $delegate(exception, cause);
      /**
       * Could add the error to a service's collection,
       * add errors to $rootScope, log errors to remote web server,
       * or log locally. Or throw hard. It is entirely up to you.
       * throw exception;
       *
       * @example
       *     throw { message: 'error message we added' };
       */
      logger.error(exception.message, errorData);
    };
  }

})();

(function() {
    'use strict';

    angular.module('app.artigo').controller('GeradorTagsController', GeradorTagsController);

	GeradorTagsController.$inject = ['$scope','$http','$log','$uibModalInstance','tags','tagsbad'];

    function GeradorTagsController($scope,$http,$log,$uibModalInstance,tags,tagsbad) {
        
		var vm = this;
		vm.clear = clear;
		vm.go = go;
		vm.finalizar = finalizar;
		vm.cancelar = cancelar;

		vm.tags = false;
		vm.comoBuscarPt = false;
		vm.comoBuscarEng = false;
		vm.anunciosPt = false;
		vm.anunciosEng = false;
		vm.tagsRevisao = false;

		vm.obj = {};
		vm.obj.tags = angular.copy(tags);
		vm.obj.tagsbad = angular.copy(tagsbad);

		vm.atual = 1;
		vm.anterior = 0;
		vm.proximo = 2;

		vm.obj.campo1 = ""; 
		vm.obj.campo2 = ""; 
		vm.obj.campo3 = ""; 
		vm.obj.campo4 = ""; 
		vm.obj.campo5 = ""; 
		vm.obj.campo6 = ""; 
		vm.obj.campo7 = ""; 
		vm.obj.campo8 = ""; 
		vm.obj.campo9 = ""; 
	    vm.obj.campo10 = "";

		function inicio(){

			go(1);
			clear();

		}

		function go(p){

			closeAll();

			if(p == 1){
				
				pagina1();

			}else if(p == 2){

				pagina2();

			}else if(p == 3){

				pagina3();

			}else if(p == 4){

				pagina4();

			}else if(p == 5){

				pagina5();

			}else if(p == 6){

				pagina6();

			}

		}

		function pagina1(){
			vm.tags = true;
			setPages(1,0,2);
		}

		function pagina2(p) {
			vm.comoBuscarPt = true;
			setPages(2,1,3);
		}

		function pagina3(p) {
			vm.comoBuscarEng = true;
			setPages(3,2,4);
		}

		function pagina4(p) {
			vm.anunciosPt = true;
			setPages(4,3,5);
		}

		function pagina5(p) {
			vm.anunciosEng = true;
			setPages(5,4,6);
		}

		function pagina6(p) {

			var uniao = vm.obj.campo1.trim() + " " + vm.obj.campo2.trim() + " " + vm.obj.campo3.trim() + " " + vm.obj.campo4.trim() + " " + vm.obj.campo5.trim() + " " + vm.obj.campo6.trim() + " " +  vm.obj.campo7.trim() + " " + vm.obj.campo8.trim() + " " + vm.obj.campo9.trim() + " " + vm.obj.campo10.trim();

			var arrayUniao = uniao.split(" ");

			arrayUniao = arrayUniao.map(function(element) {
    			var s = element.trim();

			  return {text:s};
			});

			vm.obj.tags = angular.copy(vm.obj.tags.concat(arrayUniao));

			clear();

			vm.tagsRevisao = true;
			setPages(6,5,10);
		}
		
		function setPages(atual,anterior,proxima){
			vm.atual = atual;
			vm.anterior = anterior;
			vm.proximo = proxima;
		}

		function closeAll(){
			vm.tags = false;
			vm.comoBuscarPt = false;
			vm.comoBuscarEng = false;
			vm.anunciosPt = false;
			vm.anunciosEng = false;
			vm.tagsRevisao = false;
		}

		function clear(){


			removerBrancos();
			
			removerDuplicidades();

			removerTodosTagsBad();
			
		}

		function removerTodosTagsBad(){

			var temp = angular.copy(vm.obj.tags);

			for(var i = 0 ; i < vm.obj.tagsbad.length ; i++){

				temp = removerItemLista(temp,vm.obj.tagsbad[i]);
			}

			vm.obj.tags = temp;

		};

		function removerItemLista(lista,item){

			for(var i = 0 ; i < lista.length ; i++){

				if( lista[i].text.toUpperCase() == item.text.toUpperCase()  ){
					lista.splice(i, 1);
					break;
				}

			}

			return lista;

		};

		function removerDuplicidades(){

			var temp = [];

			for(var i = 0 ; i < vm.obj.tags.length ; i++){

				if(!foiIncluido(temp, vm.obj.tags[i])){
					temp.push(angular.copy(vm.obj.tags[i]));
				}
				
			}

			vm.obj.tags = temp;

		}

		function foiIncluido(lista,termo){

			var retorno = false;
			
			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.toUpperCase() == termo.text.toUpperCase()){
					retorno = true;
					break;
				}
				
			}

			return retorno;

		}

		function removerBrancos(){

			var temp = [];

			for(var i = 0 ; i < vm.obj.tags.length ; i++){

				var p = vm.obj.tags[i].text.trim();

				if(p.length > 2 && p.length < 12){

					temp.push({text:p});
				}
				
			}

			vm.obj.tags = temp;

		}

		function finalizar(){

			$uibModalInstance.close(vm.obj.tags);

		}

		function cancelar(){
			$uibModalInstance.dismiss('cancel');
		}

		closeAll();
		inicio();
	};

})();


(function() {
    'use strict';

    angular.module('app.artigo').controller('CheckListQualidadeController', CheckListQualidadeController);

	CheckListQualidadeController.$inject = ['$scope','$http','$log','$uibModalInstance'];

    function CheckListQualidadeController($scope,$http,$log,$uibModalInstance) {
        
		var vm = this;

		vm.validations = {};





		
	};

})();


(function() {
    'use strict';

    angular.module('app.usuario', []);

})();

/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.usuario').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.usuarioatualizar',
				config: {
					url: '/usuario/atualizar',
					title: 'Atualizar Usuário',
					views: {
						'content@app': {
							templateUrl: 'usuario/usuario.atualizar.html',
							controller: 'UsuarioAtualizarController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.usuariolistar',
				config: {
					url: '/usuario/listar',
					title: 'Listar Usuários',
					views: {
						'content@app': {
							templateUrl: 'usuario/usuario.listar.html',
							controller: 'UsuarioListarController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.usuario').controller('UsuarioListarController', UsuarioListarController);

	UsuarioListarController.$inject = ['$scope','$http'];

    function UsuarioListarController($scope,$http) {
        
		var vm = this;

		vm.enviarConvite = enviarConvite;

		vm.open = false;
		vm.lista = [];
	
		vm.obj = {};

		/*
		* Função Inicio.
		*/
		function activate() {
			
			$http.get('/usuario', {}).then(function(res){

				vm.lista = res.data;

			});
						
        };

        function enviarConvite(){

       	 	$http.post('/usuario', angular.toJson(vm.obj), {}).then(function(){

       	 		
       	 	});


        }
		

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.usuario').controller('UsuarioAtualizarController', UsuarioAtualizarController);

	UsuarioAtualizarController.$inject = ['$scope'];

    function UsuarioAtualizarController($scope) {
        
		var vm = this;

		vm.obj = {};
		vm.obj.nome = "Pablo";
		vm.obj.idade = "33";
	
	
		/*
		* Função Inicio.
		*/
		function activate() {
			
				
			

        };

        vm.alertss = function(){

        


        }
		

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.login', []);

})();

/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.login').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.login',
				config: {
					url: '/',
					title: 'Login',
					views: {
						'content@app': {
							templateUrl: 'login/login.html',
							controller: 'LoginController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.login2',
				config: {
					url: '/login',
					title: 'Login',
					views: {
						'content@app': {
							templateUrl: 'login/login.html',
							controller: 'LoginController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.login').controller('LoginController', LoginController);

	LoginController.$inject = ['$scope','$http','$localStorage','$state','$rootScope', '$q'];

    function LoginController($scope,$http,$localStorage,$state,$rootScope, $q) {
        
		var vm = this;
		vm.entrar = entrar;

		vm.obj = {};
		vm.obj.email = "";
		vm.obj.senha = "";
		vm.obj.esqueci = false;

		/*
		* Função Inicio.
		*/
		function activate() {
			
						
        };

        function entrar(){

        	if(vm.obj.esqueci){

        		if(vm.obj.email){
        			esqueci();
        		}else{
        			alert("Digite o e-mail");
        		}

        	}else{

        		if(vm.obj.email && vm.obj.senha){
        			login();
        		}else{
        			alert("Digite as informações para entrar");
        		}
        		
        	}
        }

        function esqueci(){

        	$http.post('/authenticate/rememberme', angular.toJson(vm.obj), {}).then(function(res){

       	 		if(res.data.success){
       	 			alert("Email enviado!");
       	 		}else{

       	 			alert(res.data.errors[0].message);
       	 		}

       	 	});

        }

        function login(){

        	$http.post('/authenticate', angular.toJson(vm.obj), {}).then(function(res){

       	 		if(res.data.token){
       	 			$localStorage.token = res.data.token;
              $localStorage.dateL = new Date();
       	 			$state.transitionTo("app.artigo", {} );
       	 		}else{

       	 			alert(res.data.errors[0].message);
       	 		}

       	 	});

        }
		
		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.imagem', []);

})();

/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.imagem').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.imagem',
				config: {
					url: '/imagens',
					title: 'Imagens',
					views: {
						'content@app': {
							templateUrl: 'imagem/imagem.html',
							controller: 'ImagemController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.categoria', []);

})();

(function() {
    'use strict';

    angular.module('app.categoria').controller('ImagemController', ImagemController);

	ImagemController.$inject = ['$scope','$http','usSpinnerService','$uibModal'];

    function ImagemController($scope,$http,usSpinnerService,$uibModal) {
        
		var vm = this;
		vm.enviar = enviar;
		vm.visualizarImagem = visualizarImagem;

		vm.obj = {};
		
		vm.lista = [];

		/*
		* Função Inicio.
		*/
		function activate() {
			
			listarImagens();
						
        };

        function listarImagens(){

        	$http.get('/imagem', {} ).then(function(res){

				vm.lista = res.data;

			});

        };

        function enviar(){

        	startSpin();

        	$http.post('/imagem', angular.toJson(vm.obj), {} ).then(function(res){

       	 		stopSpin();

       	 		if(res.data.erro){
       	 			toastr.error(res.data.erro, 'Ocorreu um erro!')
       	 		}else{
       	 			toastr.success('Suas imagens foram inseridas.', 'Tudo certo!')
       	 		}

       	 	});

        }
		
		function visualizarImagem(id){

			var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'imagem/visualizar-imagem/visualizar.imagem.html',
		      	controller: 'VisualizarImagemController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	id: function () {

			        	return id;

		        	}

		      	}
		    });	


     		modalInstance.result.then(function () {
	      		
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

		}

		function startSpin() {

	        usSpinnerService.spin('spinner-1');
	       
	    };

	    function stopSpin() {
	      
	        usSpinnerService.stop('spinner-1');
	      
	    };

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.directives', [

    	'sidebar.directive'


    ]);
	
})();

(function() {
    'use strict';

    angular.module('app.curso', []);

})();

/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.curso').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.curso',
				config: {
					url: '/cursos',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'app/curso/curso.html',
							controller: 'CursoController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.curso').controller('CursoController', CursoController);

	CursoController.$inject = ['$scope'];

    function CursoController($scope) {
        
		var vm = this;

		vm.obj = {};
		vm.obj.nome = "curso!!!!"

		vm.states = [{'abbrev':'RJ'},{'abbrev':'SP'},{'abbrev':'TO'}];
		/*
		* Função Inicio.
		*/
		function activate() {
			
						
        };
		

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.core', [
        'ui.router',
		'blocks.router',
		'ngAnimate',
	
		'textAngular',
		'naif.base64',
		'angularSpinner',
		'datatables',
		'ngStorage',
		'ngTagsInput',
		'ui.bootstrap'
    ]);


    

})();

/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.categoria').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.categoria',
				config: {
					url: '/categorias',
					title: 'Categorias',
					views: {
						'content@app': {
							templateUrl: 'categoria/categoria.html',
							controller: 'CategoriaController',
							controllerAs: 'vm'
						}
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.categoria').controller('CategoriaController', CategoriaController);

	CategoriaController.$inject = ['$scope'];

    function CategoriaController($scope) {
        
		var vm = this;

		vm.obj = {};
		vm.obj.nome = "FUNCIONANDO!!!!"

		vm.states = [{'abbrev':'RJ'},{'abbrev':'SP'},{'abbrev':'TO'}];
		/*
		* Função Inicio.
		*/
		function activate() {
			
						
        };
		

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app.artigo').controller('IncluirArtigoController', IncluirArtigoController);

	IncluirArtigoController.$inject = ['$scope','$http','$log','$uibModal','artigo','categorias'];

    function IncluirArtigoController($scope,$http,$log,$uibModal,artigo,categorias) {
        
		var vm = this;
		vm.incluir = incluir;
		vm.sugerir = sugerir;
		vm.abrirGeradorTags = abrirGeradorTags;
		vm.abrirCheckListQualidade = abrirCheckListQualidade;


		vm.isCollapsedTitulo = false;
		vm.isCollapsedSubTitulo = false;

		vm.taOptions = [
		      ['html'],['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'pre' ],
		      ['bold', 'italics', 'underline', 'ul', 'ol'],
		      ['justifyCenter', , 'indent', 'insertImage','insertLink', 'insertVideo', 'charcount'],
	  	];

		vm.obj = {};
		vm.obj.titulo = "";
		vm.obj.subtitulo = "";
		vm.obj.textHtml = "";
		vm.obj.url =  "";
		vm.obj.categoria = 0;
		vm.obj.tags = [];
		vm.obj.images = null;

		vm.categorias = [];


        vm.validations = {
        	url:false
        }

        vm.conteudoInicial = '';
		vm.conteudoInicial = vm.conteudoInicial + '<h3>​Titulo de Sessão - usando &lt;h3&gt;</h3>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>O conteúdo deverá ser dividido em parágrafos e cada um destes escrito utilizando a tag &lt;p&gt;. Para os textos é importante sempre usar a tag &lt;p&gt;. É importante respeitar a ortografia. Os autores estão livres para utilizar vídeos do YouTube, imagens externas ou links para outras páginas.</p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>É necessário que todas as<b> palavras ou trechos importantes do conteúdo estejam em negrito </b><u>ou subinhad</u><u>o.</u></p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p style="text-align: center;"><i>Tente evitar muitos espaços​ ou itens centralizados!</i></p>';
		vm.conteudoInicial = vm.conteudoInicial + '<p>Códigos precisam estar entre as tags &lt;pre&gt; e &lt;code&gt; e especificando a linguagem usada. Exemplo:</p>';

vm.conteudoInicial = vm.conteudoInicial + '<pre><code class="language-java">';
vm.conteudoInicial = vm.conteudoInicial + '\n';
vm.conteudoInicial = vm.conteudoInicial + 'public class Blog{	\n';	
vm.conteudoInicial = vm.conteudoInicial + '	private String teste;	\n';	
vm.conteudoInicial = vm.conteudoInicial + '	public String getTeste(){	\n';		
vm.conteudoInicial = vm.conteudoInicial + '		return teste;		\n';
vm.conteudoInicial = vm.conteudoInicial + '	}	\n';
vm.conteudoInicial = vm.conteudoInicial + '}\n';
vm.conteudoInicial = vm.conteudoInicial + '\n';
vm.conteudoInicial = vm.conteudoInicial + '</code>​</pre>\n';



		/*
		* Função Inicio.
		*/
		function activate() {
			
			vm.categorias = angular.copy(categorias);

			vm.obj.textHtml = vm.conteudoInicial;

        };
		
		/*
		* Incluindo novo Artigo no Sistema
		*/
        function incluir(){
        	
        	vm.obj.img = angular.copy(vm.obj.imgTmp);
        	
       	 	$http.post('/artigos/', angular.toJson(vm.obj), {}).then(function(){
       	 		toastr.info('Inserido com sucesso!');	
       	 	});

        };

 
        /*
		* Sugerindo uma nova URL para o artigo.
		*/
        function sugerir(){
        	
        	if(vm.obj.titulo.length > 10){

	        	var titulo = angular.copy(vm.obj.titulo);
	        	var pos = titulo.indexOf(" ");
			    
			    while (pos > -1){
					titulo = titulo.replace(" ", "-");
					pos = titulo.indexOf(" ");
				}
			    
				vm.obj.url = titulo.toLowerCase();
        	}else{
        		toastr.info('Titulo do artigo esta muito pequeno');
        	}
        	
        }

        /*
		* Abrir a aplicação de Geracao de tags
		*/
        function abrirGeradorTags(){
 
		   	var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'artigo/geradorTags/gerador.tags.html',
		      	controller: 'GeradorTagsController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	tags: function () {

		        		if(!vm.obj.tags || vm.obj.tags.length == 0){

			        		var arrayTitulo = vm.obj.titulo.trim().split(" ");

			        		arrayTitulo = arrayTitulo.map(function(element){
			        			var s = element.trim();

							  return {text:s};
							});

			        		var arraySubTitulo = vm.obj.subtitulo.trim().split(" ");

			        		arraySubTitulo = arraySubTitulo.map(function(element) {
			        			var s = element.trim();

							  return {text:s};
							});

			        		var array = arrayTitulo.concat(arraySubTitulo);


			        		array = removerBrancos(array);


			          		return removerDuplicidades(array);

		        		}else{

		        			return vm.obj.tags;

		        		}
		        		
		        	},

		        	tagsbad: function(){

		        		return $http.get('/tagsbad', {}).then(function(res){

							return res.data;

						});

		        	}

		      	}
		    });	


     		modalInstance.result.then(function (tags) {
	      		vm.obj.tags = tags;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

        }


        /*
		* Abrir o modal de qualidade.
		*/
        function abrirCheckListQualidade(){
 
		   	var modalInstance = $uibModal.open({
		      	animation: true,
		      	ariaLabelledBy: 'modal-title',
		      	ariaDescribedBy: 'modal-body',
		      	templateUrl: 'artigo/checkListQualidade/checklist.html',
		      	controller: 'CheckListQualidadeController',
		      	controllerAs: 'vm',
		      	size: 'lg',
		      	resolve: {
		        	
		        	url: function () {

		        		if(vm.obj.url.length > 50){
			        		
			        		return {success:false,msg:"A URL esta muito grande."};

			        	}else{
				       	 	
				       	 	$http.post('/artigos/verificarURL', angular.toJson({url:vm.obj.url}), {}).then(function(res){
				       	 		return {success:res.data.result,msg:"URL existente."};
				       	 	});

			       	 	}
		        		
		        	},

		        	artigo: function(){

						return vm.obj;

		        	}

		      	}
		    });	


     		modalInstance.result.then(function (tags) {
	      		vm.obj.tags = tags;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });

        }


        /*
		* Funcoes auxiliares
		*/
        function removerDuplicidades(lista){

			var temp = [];

			for(var i = 0 ; i < lista.length ; i++){

				if(!foiIncluido( temp, lista[i] )){
					temp.push(angular.copy(lista[i]));
				}
				
			}

			return temp;

		}
		function removerBrancos(lista){

			var temp = [];

			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.trim().length > 0){
					temp.push(angular.copy(lista[i]));
				}
				
			}

			return temp;

		}
		function foiIncluido(lista,termo){

			for(var i = 0 ; i < lista.length ; i++){

				if(lista[i].text.toUpperCase() == termo.text.toUpperCase()){
					return true;
				}
				
			}

			return false;

		}






		// Iniciando o procedimento.
		activate();
		





	};

})();


/**
 * Created by Victor Maia Sarrasqueiro
 */

(function() {

    'use strict';

    angular.module('app.artigo').run(appRun);

    appRun.$inject = ['routerHelper','$stateParams','$http'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    };

    function getStates() {
        return [
		
			{
				state: 'app.artigo',
				config: {
					url: '/artigos',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/artigo.html',
							controller: 'ArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						lista: function($stateParams,$http){

							return $http.get('/artigos').then(function(res){
							
								return res.data;

							}); 
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{
				state: 'app.incluirartigo',
				config: {
					url: '/artigos/incluir',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/incluir.artigo.html',
							controller: 'IncluirArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						artigo: function($stateParams){

							return null; 
							
						},
						categorias: function($http){

							return $http.get('/categoria', {}).then(function(res){

								return res.data;

							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			},
			{	state: 'app.editarartigo',
				config: {
					url: '/artigos/editar/:id',
					title: 'Home',
					views: {
						'content@app': {
							templateUrl: 'artigo/incluir.artigo.html',
							controller: 'IncluirArtigoController',
							controllerAs: 'vm'
						}
					},
					resolve:{

						artigo: function($stateParams,$http){

							var url = '/artigos/' + $stateParams.id;
							return $http.get(url).then(function(res){
							
								return res.data;

							}); 
							
						},
						categorias: function($http){

							return $http.get('/categoria', {}).then(function(res){

								return res.data;

							});
							
						}
						
					},
					data: {
						permissions: {
							only: ['administrator']
						}
					}
				}
	
			}
		
		];
    };

})();

(function() {
    'use strict';

    angular.module('app.artigo').controller('ArtigoController', ArtigoController);

	ArtigoController.$inject = ['$scope','$http','$log','$uibModal','$state', 'lista'];

    function ArtigoController($scope,$http,$log,$uibModal,$state,lista) {
        
		var vm = this;
		vm.edit = edit;

		vm.artigos = [];

		function activate(){

			vm.artigos = angular.copy(lista);

		}

		function edit(idC){

			$state.transitionTo("app.editarartigo", { 'id':idC} );
		};

		// Iniciando o procedimento.
		activate();
		
	};

})();


(function() {
    'use strict';

    angular.module('app', [
		'app.directives',
        'app.core',
        'app.categoria',
        'app.curso',
        'app.usuario',
		'app.artigo',
        'app.imagem',
		'app.login'
        
    ]);


    angular.module('app').config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });

    angular.module('app').factory('AuthInterceptor', function ($rootScope, $q, $localStorage,$location) {
       
        'use strict';
        
        return {

            request: montarCabecalhoReq,
            response: recuperarCabecalhoRes

        };

        function montarCabecalhoReq(config) {

            config.headers = config.headers || {};
            config.headers['Content-Type'] = 'application/json';
            
            var token = $localStorage.token;

            if (token){
                config.headers.Authorization = 'Bearer ' + token;
            }

            return config;
        }

        function recuperarCabecalhoRes(response){

            var deferred = $q.defer();

            if( response.data.code && ( response.data.code == 401 || response.data.code == 403) ){
             
                $location.path('/login');
            
            }

            deferred.resolve(response);
            
            return deferred.promise;

        }

    });


})();
(function() {
  'use strict';

  angular.module('app').run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    var otherwise = '/404';
    routerHelper.configureStates(getTemplates());
    routerHelper.configureStates(getHttpErros(), otherwise);
  }

  function getHttpErros() {
    return [{
      state: '404',
      config: {
        url: '/404',
        title: '404',
        templateUrl: 'core/404.html'
      }
    }];
  }

  function getTemplates() {
    return [
      {
        state: 'app',
        config: {
          views: {
            '': {
              templateUrl: 'template/template.app.html'
            },
            'header@app': {
              templateUrl: 'template/layout/header.html'
            }
          }
        }
      }
    ];
  }

})();
(function() {
  'use strict';

  angular.module('app').controller('AppController', AppController);

  AppController.$inject = ['$scope'];

  function AppController($scope) {
    
	var vm = this;

  }

})();
